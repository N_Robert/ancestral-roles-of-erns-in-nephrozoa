Stage groups:

**DROME**

 * Embryo: em0-2hr, em2-4hr, em4-6hr, em6-8hr, em8-10hr, em10-12hr, em12-14hr, em14-16hr, em16-18hr, em18-20hr, em20-22hr, em22-24hr
 * Larva: L1, L2, L3\_12hr, L3\_PS1-2, L3\_PS3-6, L3\_PS7-9
 * Metamorphosis/Early Juv.: WPP, P5, P6, P8, P9]0, P15
 * Late Juv./Adult: AdF\_Ecl\_1days, AdF\_Ecl\_5days, AdF\_Ecl\_30days, AdM\_Ecl\_1days, AdM\_Ecl\_5days, AdM\_Ecl\_30days

**LITVA**

 * Embryo: Zygote, Blastula, Gastrula, Limb\_Bud\_Embryo, Prehatching\_Larva
 * Larva: Nauplius, Zoea, Mysis
 * Metamorphosis/Early Juv.: Postlarva  
 * Late Juv./Adult: Adult\_Inter\_Molt, Adult\_Pre\_Molt\_0, Adult\_Pre\_Molt\_1, Adult\_Pre\_Molt\_2, Adult\_Pre\_Molt\_3, Adult\_Pre\_Molt\_4, Adult\_Post\_Molt\_1, Adult\_Post\_Molt\_2

**PLADU**

 * Embryo: hr0, hr2, hr4, hr6, hr8, hr10, hr12, hr14
 * Larva: hr24, hr36, hr48, hr72
 * Metamorphosis/Early Juv.: d4, d10, d15, m1Pre, m1-Post
 * Late Juv./Adult: m3, male, female

**HALDIHA**

 * Embryo: 01\_8\_cell, 02\_Morula, 03\_Gastrula
 * Larva: 04\_Early\_trochophore, 05\_Trochophore, 06\_Late\_trochophore, 07\_Veliger
 * Metamorphosis/Early Juv.: 08\_Pediveliger
 * Late Juv./Adult: 09\_Juvenile

**CRAGI**

 * Embryo: Egg, 01\_2\_cell, 02\_4\_cell, 03\_Early\_morula, 04\_Morula, 05\_Blastula, 06\_Rotary\_movement
 * Larva: 07\_Free\_swimming, 08\_Early\_gastrula, 09\_Gastrula, 10\_Trochophore\_1, 11\_Trochophore\_2, 12\_Trochophore\_3, 13\_Trochophore\_4, 14\_Trochophore\_5, 15\_Early\_D\_shape\_larva\_1, 16\_Early\_D\_shape\_larva\_2, 17\_D\_shape\_larva\_1, 18\_D\_shape\_larva\_2, 19\_D\_shape\_larva\_3, 20\_D\_shape\_larva\_4, 21\_D\_shape\_larva\_5, 22\_D\_shape\_larva\_6, 23\_D\_shape\_larva\_7, 24\_Early\_umbo\_larva\_1, 25\_Early\_umbo\_larva\_2, 26\_Umbo\_larva\_1, 27\_Umbo\_larva\_2, 28\_Umbo\_larva\_3, 29\_Umbo\_larva\_4, 30\_Umbo\_larva\_5, 31\_Umbo\_larva\_6, 32\_Later\_umbo\_larva\_1, 33\_Later\_umbo\_larva\_2
 * Metamorphosis/Early Juv.: 34\_Pediveliger\_1, 35\_Pediveliger\_2, 36\_Spat
 * Late Juv./Adult: 37\_Juvenile

**MIZYE**

 * Embryo: Zyg, 2-8cell, Bla, Gas
 * Larva: Tro, Dst, E-umb, M-umb, P-umb
 * Metamorphosis/Early Juv.: Ped, Spa, Juv, Adu-1y
 * Late Juv./Adult: Adu-2y, Adu-3y, Adu-4y

**ANNJA**

 * Embryo: Unfert\_Eggs, 2\_Cells, 8\_Cells, 32\_Cells, Gastrula
 * Larva: Hatching, Early\_Doliolaria, Doliolaria
 * Metamorphosis/Early Juv.: Attachment\_Stage, Early\_Cystidean, Late\_Cystidean, Early\_Pentactula
 * Late Juv./Adult: Late\_Pentactula, Juvenile, Adult

**APOJA**

 * Embryo: Fert\_Eggs, 4\_Cells, Morula\_6hpf, Blastula\_14hpf
 * Larva: Gastrula\_29hpf, Late\_Gastrula, Early\_Auriclaria, Mid\_Auriclaria, Late\_Auriclaria
 * Metamorphosis/Early Juv.: Metamorph1\_17d, Metamorph2, Metamorph3, Metamorph4\_19d, Doliolaria\_19d, Pentactula\_27d
 * Late Juv./Adult: Juvenile\_51d

**HELER**

 * Embryo: Unfert\_Eggs, 4\_Cells, 16\_Cells, 32\_Cell, Mesenchyme\_Blastula
 * Larva: Gastrula\_24hpf, Vestibular\_Larva, Early\_Rudiment, Pentaradial\_Rudiment, Advanced\_Rudiment
 * Metamorphosis/Early Juv.: Early\_Juvenile
 * Late Juv./Adult: Late\_Juvenile