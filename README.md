This is the scripts associated with the manuscript "Ancestral role of ecdysis-related neuropeptides in bilaterian life cycle transitions"

# Data preparation scripts:
## Convert *D. melanogaster* values RPKM to TPMs
delete the data from the 4 genes missing data for embryonic stages 
grep modENCODE_mRNA-Seq_U DROME_bulk_fbkms.tsv |cut -f2,7,8 | grep -vP "FBgn0267596|FBgn0267594|FBgn0267434|FBgn0267595" > DROME_RPKM.tsv

Use`DROME_RPKMs_to_tpms.py` to convert RPKM to tpms.

## Convert *R. venosa* read counts to TPMs
The script `Rapena_readcounts_to_tmp.py` uses length info with the `GSE70548_Quantification.txt` to convert read counts by transcripts to RPKMs.
RPKMS are then converted to TPMs

# Scripts to generate figures
## Plot heatmaps
With <tpm_table> the full tpms tables of ERNs for a given species (median tmp by stage, if multiple replicates), the following command was used to create the heatmaps
`make_heatmap.py -t 0.5 <tpm_table>`
Row-wise normalisation is done by zscore.
## correlation analysis
For each species included in figure S3 <full_tpm> the full TPM values of all the transcripts, <annotation> a tab-separated file with two fields, one line by ERN, field 1 is gene name, field2 is accession.
`correlation_analysis.py` -t <full_tpm> -a  <annotation> -o <output_file_name>

## line graphs and boxplots
Tables with max tpm by stage_groups in `data_merged_stages/` Were used as source files, stages grouped according to `data_merged_stages/Merged_stages_info.md`
The following command was used.
Each stage_group TPM maximumum was normalized as a fraction of the max tpm of the whole developmental series
```
ERN_expr_life_his.py data_merged_stages/ERN_by_life_history_direct_devs.tsv data_merged_stages/Line_graphs_direct_devs.pdf data_merged_stages/Boxplot_direct_devs.pdf data_merged_stages/Tests_indirect_devs.tsv

ERN_expr_life_his.py data_merged_stages/ERN_by_life_history_indirect_devs.tsv data_merged_stages/Line_graphs_indirect_devs.pdf data_merged_stages/Boxplot_indirect_devs.pdf data_merged_stages/Tests_direct_devs.tsv
```