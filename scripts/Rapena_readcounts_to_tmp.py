#! /usr/bin/python3

import pandas as pd

data_df = pd.read_csv("GSE70548_Quantification.txt", sep='\t')

#Isolate only read_counts columns from original df
read_count_sample_ls = [name for name in data_df.columns if name.startswith('Read_count')]

#This is our tpm
TPM_full_df = data_df[['gene_id']]

for sample in read_count_sample_ls:
	RPK_Series = data_df[sample]/(data_df['Gene Length']/1000) #Temporary series with reads per kilobase
	TPM_df[sample] = RPK_Series/(data_df['Read_count(C1_1)'].sum()/1000000) #scale RPK with a "per million" factor.

sample_names_ls = ['C1','D2','F3','G3','J4','Y5']

TPM_median_df = TPM_df[['gene_id']]
for stage in sample_names_ls:
	TPM_median_df[stage] = TPM_df[[name for name in TPM_df.columns if stage in name]].median(axis = 1)

with open('GSE70548_Quantification_median_of_TPMs.tsv', 'w') as f:	
	f.write(TPM_median_df.to_csv(sep = '\t', index = False))