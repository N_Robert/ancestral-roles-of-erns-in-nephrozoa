#!/usr/bin/env python3

import argparse
import scipy
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

parser = argparse.ArgumentParser(description = """Makes a heatmap out of the provided table""")
parser.add_argument('-b', '--bar_label', help = 'Caption of the legend bar.', default = 'zscore')
parser.add_argument('input', help = 'input table. Columns are stages, rows are transcripts')
parser.add_argument('-s', '--title', help = 'title of the plot. Default is args.input.')
parser.add_argument('-o', '--output', help = 'name of the output file, default name is as follows: if input is "inputname.tmps.tsv" output is "inputname_heatmap.pdf')
parser.add_argument('-t', '--threshold', help = 'Mask tpms under certain threshold.', type = float, default = 1)
parser.add_argument('-r', '--rectangles', help = 'If the flag is used, each field of the heatmap a rectangle (default: square). In the case of clustermap, no squares', action = 'store_false')
parser.add_argument('--width', help = 'width of the figure, in inches', default = 8)
parser.add_argument('--height', help = 'height of the figure, in inches', default = 6)
args = parser.parse_args()


if args.title is None:
    args.title = args.input
if args.output is None:
    output_prefix = args.input.split('.')[0]
    args.output = f'{output_prefix}_{args.threshold}masked_heatmap.pdf'

print(f'Heatmap will be saved under {args.output}')

df = pd.read_csv(args.input, index_col=0, sep = '\t')

df_mask = df.apply(lambda x: x < args.threshold)

z_score_df = df.apply(scipy.stats.mstats.zscore, axis = 1, result_type = 'expand')

# apply zscore normalization to every row (axis = 1)
#result_type expand makes an output into columns. If not, we end up with 2 columns, one with names, one with the array of results
z_score_df.columns = list(df.columns.values)

g = sns.clustermap(data = z_score_df,
                linewidths = .15,
                square = False,
                center = 0,
                cmap = 'RdYlBu_r',
                vmin = -4,
                vmax = 4, # maximum values on the colorbar
                mask = df_mask,
                col_cluster = False,
                yticklabels = True,
                figsize = (args.width, args.height),
                cbar_kws={'label': args.bar_label, 'shrink': .3, 'ticks':list(range(-4,5,2))})# dictionary for colorbar. shrink it so that it doesn't look too big next to the heatmap
g.ax_heatmap.set_facecolor('#CCCCCC')
g.ax_heatmap.set_title(f'{args.title}', fontsize = 10)
g.ax_heatmap.set_xticklabels(g.ax_heatmap.get_xmajorticklabels(), fontsize = 5)
g.ax_heatmap.xaxis.set_ticks_position('none') 
g.ax_heatmap.set_yticklabels(g.ax_heatmap.get_ymajorticklabels(), fontsize = 5)
g.ax_heatmap.yaxis.set_ticks_position('none') 
g.savefig(args.output)

