#!/usr/bin/env python3

import sys

if len(sys.argv) ==0:
    print("""Usage
    python3 ERN_expr_life_his.py ERN_by_life_history.tsv
    makes line graphs plots and one boxplot, all in one""")

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib as mpl
import scipy.stats
import itertools

mpl.rcParams['xtick.labelsize'] = 8
mpl.rcParams['ytick.labelsize'] = 8
sns.set_style('ticks')

data = sys.argv[1]
line_graph = sys.argv[2]
boxplot = sys.argv[3]
tests_stages = sys.argv[4]


df = pd.read_csv(data, sep = '\t', names = ['SPECIES', 'para_index', 'ern', 'max_tpm', 'stage_group'])

df['gene'] = df.SPECIES.map(str) + '_' + df.ern.map(str) + '_' + df.para_index.map(str)


ern_ls = ['EH', 'CCAP', 'MIP', 'BURA', 'BURB']

indirect_colors = ['#3cb44b', '#42d4f4', '#4363d8', '#911eb4', '#a9a9a9', '#e6194B', '#f58231', '#fabed4', '#ffe119']
direct_colors = ['#800000', '#9A6324', '#808000', '#469990', '#000075', '#dcbeff']

if '2_Larva' in set(df.stage_group):
    color_mapping_boxplot = {'1_Embryo': '#2D65B0', '2_Larva': '#262262', '3_Metamorphosis/Early_Juv.': '#CD6435' , '4_Late_Juv./Adult' : '#231F20'}
    color_mapping = dict(zip(set(df.SPECIES), indirect_colors))
else:
    color_mapping_boxplot = {'Embryo': '#2D65B0', 'Hatching/Early_Juv.': '#CD6435' , 'Late_Juv./Adult' : '#231F20'}
    color_mapping = dict(zip(set(df.SPECIES), direct_colors))


print(color_mapping)

df_norm = pd.DataFrame(columns = ['SPECIES', 'para_index', 'ern', 'max_tpm', 'stage_group'])

gene_ls = set(df.gene)
for gene in gene_ls:
    df_gene = df.query('gene == @gene')
    max_dev_tmp = df_gene['max_tpm'].max()
    df_gene['max_tpm'] = df_gene['max_tpm'] / max_dev_tmp
    df_norm = pd.concat([df_norm, df_gene])


df_norm.sort_index(inplace = True)

fig, axs = plt.subplots(5,1, sharex = True, sharey = True, figsize = (4,8))

for i in range(0,5):
    ern = ern_ls[i]
    df_ern = df_norm.query('ern == @ern')
    if i == 0:
        legend_status = 'brief'
    else:
        legend_status = False
    sns.lineplot(x = 'stage_group',
             y = 'max_tpm',
             hue = 'SPECIES',
             palette = color_mapping,
             legend = legend_status,
             estimator = None,
             units = 'para_index',
             data = df_ern,
             ax = axs[i])
    axs[i].axes.set_xlabel(None)
    axs[i].axes.set_ylabel(ern)

plt.xticks(rotation = -45)
sns.despine()
plt.savefig(line_graph)

fig, ax = plt.subplots(figsize = (4,4))

sns.boxplot(x = 'stage_group',
            y = 'max_tpm',
            palette = color_mapping_boxplot,
            data = df_norm,
            medianprops = {'color':'red', 'linewidth':'1.5'},
            ax = ax)

ax.set_ylim(0,1)
plt.xticks(rotation = -45)
sns.despine()
plt.savefig(boxplot)

df_norm_stage_group_ls = sorted(list(set(df_norm.stage_group)))

header = ['stage1', 'stage2', 'median_stage1', 'median_stage2', 'wilcox_rank_sum_test']
results = []
for stage1,stage2 in itertools.combinations(df_norm_stage_group_ls, 2):
    stage1_values = df_norm.query('stage_group == @stage1').max_tpm
    stage2_values = df_norm.query('stage_group == @stage2').max_tpm
    pvalue = scipy.stats.mannwhitneyu(stage1_values, stage2_values)[1]
    results.append([stage1,
                    stage2,
                    stage1_values.median(),
                    stage2_values.median(),
                    pvalue])


bh_df = pd.DataFrame(results, columns = header)
bh_df.sort_values('wilcox_rank_sum_test', inplace = True)

bh_df['rank'] = range(1, len(bh_df)+1)
bh_df['fdr_bh'] = (bh_df['rank'] / len(bh_df)) * 0.05
bh_df['significant'] = bh_df.wilcox_rank_sum_test < bh_df.fdr_bh

bh_df.to_csv(tests_stages, sep = '\t', index = False)




