#! /usr/bin/env python3

import argparse
import numpy as np
import scipy.stats
import itertools
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import collections

parser = argparse.ArgumentParser(description = 'correlation analysis, compares ')
parser.add_argument('-t', '--full_tpm_table', help = 'tpm table', required = True)
parser.add_argument('-a', '--annotation', help = 'tab-separated file, field 1 comprises neuropeptide accessions (accessions in TMP table), field 2 should be annotation', required = True)
parser.add_argument('-n', '--random', help = 'number of random samples to use, default 1000', default = 1000, type = int)
parser.add_argument('-f', '--FDR', help = 'False discovery rate of benjamini hochberg procedure', type = float, default = 0.05)
parser.add_argument('-o', '--output', help = 'name of the output file', type = str)
args = parser.parse_args()

#mystuff = collections.namedtuple('mystuff', ['full_tpm_table', 'annotation', 'random', 'FDR', 'output'])
#args = mystuff('../TPM_normalizations/DROME/DROME_filt_tpms.tsv', '/scratch/zieger/Expr_Correlation/DROME.ERNs.list', 1000, 0.05, 'DROME_by_session')

#load data
tpm_dict = {}

with open(args.full_tpm_table, 'r') as f:
    header = f.readline().rstrip().split('\t')
    for line in f:
        line = line.rstrip().split('\t')
        if header[1] == 'length':
            tpm_dict[line[0]] = np.array([np.float(x) for x in line[2:]])
        else:
            tpm_dict[line[0]] = np.array([np.float(x) for x in line[1:]])

study_acc_ls = []
study_genes_ls = []
annotation_dict = {}
with open(args.annotation, 'r') as f:
    for line in f:
        accession, annotation, *_ = line.rstrip().split('\t')
        study_acc_ls.append(accession)
        study_genes_ls.append(annotation)
        annotation_dict[accession] = annotation

#delete study from background sequences
background_ls = np.array([x for x in tpm_dict.keys() if x not in study_acc_ls])

#Calculate pairwise corr of genes within the study
study_corr_results_ls = []
study_corr_results_header = ['gene1', 'gene2', 'acc_gene1', 'acc_gene2', 'r', 'pvalue']
for acc_pair in itertools.combinations(study_acc_ls, 2):
    correlation, pvalue = scipy.stats.spearmanr(tpm_dict[acc_pair[0]], tpm_dict[acc_pair[1]])
    study_corr_results_ls.append([annotation_dict[acc_pair[0]],
                                  annotation_dict[acc_pair[1]],
                                  acc_pair[0],
                                  acc_pair[1],
                                  correlation,
                                  pvalue])

study_corr_results_df = pd.DataFrame(study_corr_results_ls, columns = study_corr_results_header)

background_corr_ls = []
##Calculate pairwise corr of genes within n random samples
#If one of the genes of the sample isn't expressed, it's discared and a new sample is picked
maxsize_corr_ls = args.random * len(study_corr_results_ls) + 1
while len(background_corr_ls) < maxsize_corr_ls:
    background_sample = np.random.choice(background_ls, size = len(study_acc_ls), replace = False)
    tmp_corr_ls = []
    ignore_sample = False
    for acc_pair in itertools.combinations(background_sample, 2):
        tpms_gene1 = tpm_dict[acc_pair[0]]
        tpms_gene2 = tpm_dict[acc_pair[1]]
        correlation, pvalue = scipy.stats.spearmanr(tpms_gene1, tpms_gene2)
        invalid_sample = [max(tpms_gene1) < 0.5,
                          max(tpms_gene2) < 0.5]
        if any(invalid_sample) is True:
            ignore_sample = True #ignore sample if one of the genes isn't expressed
        if np.isnan(correlation) == False:
            tmp_corr_ls.append(correlation)
    #if the length of the background doesn't match the study ignore it
    if ignore_sample is True or len(tmp_corr_ls) != len(study_corr_results_ls):
        pass
    else:
        background_corr_ls.extend(tmp_corr_ls)
#For some reason, the while loop doesn't necessarily stop at a 100 (often 102-103)...
# we rrim down the list.
background_corr_ls = background_corr_ls[0:(args.random * len(study_corr_results_ls))] 

#Overall stats
study_med = np.median(study_corr_results_df.r)
background_med = np.median(np.array(background_corr_ls))
pvalue = scipy.stats.mannwhitneyu(study_corr_results_df.r, background_corr_ls)[1]
corr_values = '\t'.join([str(x) for x in study_corr_results_df.r])
pvalue_corr_study_str = '\t'.join([str(x) for x in study_corr_results_df.pvalue])


bh_df = study_corr_results_df.sort_values(by = 'pvalue', inplace = False, ascending = True)
bh_df['rank'] = range(1, len(bh_df)+1)
bh_df['fdr_bh'] = (bh_df['rank'] / len(bh_df)) * args.FDR
bh_df['significant'] = bh_df.pvalue < bh_df.fdr_bh

bh_df.sort_values(by = 'r', inplace = True, ascending = False)
bh_df.to_csv(f'{args.output}_corr.log', index = False, sep = '\t')

with open(f'{args.output}_corr.log', 'a') as f:
    f.write(f"""
    study median corr\t{study_med}
    background median corr\t{background_med}
    pvalue wilcox rank-sum\t{pvalue}""")

heatmap_df = pd.DataFrame(columns = study_genes_ls, index = study_genes_ls, data = 0.0)
mask_df = pd.DataFrame(columns = study_genes_ls, index = study_genes_ls, data = False)


#fill up dataframe with correlation values
corr_dict = {frozenset([gene1,gene2]): r for gene1, gene2, r in zip(bh_df.gene1, bh_df.gene2, bh_df.r)}
for genea,geneb in itertools.permutations(study_genes_ls, 2):
    heatmap_df.at[genea,geneb] = corr_dict[frozenset([genea,geneb])]


#mask = True if triangle
for x,y in zip(np.triu_indices_from(mask_df)[0],np.triu_indices_from(mask_df)[1]):
    mask_df.at[study_genes_ls[x], study_genes_ls[y]] = True


fig,ax = plt.subplots()
ax = sns.heatmap(data = heatmap_df,
                ax = ax,
                linewidths = .15,
                square = True,
                center = 0,
                cmap = 'RdYlBu_r',
                mask = mask_df,
                fmt = '.1',
                vmin = -1,
                vmax = 1, # maximum values on the colorbar
                annot_kws={"size": 20},
                cbar_kws={'label': 'corr', 'shrink': .3, 'ticks':[-1, 0, 1]})# dictionary for colorbar. shrink it so that it doesn't look too big next to the heatmap
bottom, top = ax.get_ylim()
ax.set_xticklabels(ax.get_xmajorticklabels(), fontsize = 10)
ax.xaxis.set_ticks_position('none') 
ax.set_yticklabels(ax.get_ymajorticklabels(), fontsize = 10)
ax.yaxis.set_ticks_position('none')
plt.tight_layout()
plt.savefig(f'{args.output}_corr_heatmap.pdf')