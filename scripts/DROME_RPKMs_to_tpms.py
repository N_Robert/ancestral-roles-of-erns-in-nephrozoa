#! /usr/bin/env python3

import itertools
import pandas as pd

results = []

"""
grep modENCODE_mRNA-Seq_U DROME_bulk_fbkms.tsv |cut -f2,7,8 | grep -vP "FBgn0267596|FBgn0267594|FBgn0267434|FBgn0267595" > DROME_RPKM.tsv
"""
with open('DROME_RPKM.tsv', 'r') as f:
    for key,group in itertools.groupby(f, lambda l: l.split()[0]):
        acc = key
        results.append([key] + [subgroup.split()[2] for subgroup in group])

stage_ls = ['gene_id','em0-2hr','em2-4hr','em4-6hr','em6-8hr','em8-10hr','em10-12hr','em12-14hr','em14-16hr','em16-18hr','em18-20hr','em20-22hr','em22-24hr','L1','L2','L3_12hr','L3_PS1-2','L3_PS3-6','L3_PS7-9','WPP','P5','P6','P8','P9]0','P15','AdF_Ecl_1days','AdF_Ecl_5days','AdF_Ecl_30days','AdM_Ecl_1days','AdM_Ecl_5days','AdM_Ecl_30days']

results_df = pd.DataFrame(results, columns = stage_ls).apply(pd.to_numeric, errors='ignore') #convert everything to numeric, will fail with gene_ids, but we ignore

for stage in stage_ls[1:]:
    rpkm_sum = results_df[stage].sum()
    results_df[stage] = results_df[stage].apply(lambda x : x/rpkm_sum * 1000000) #transform RPKMs to TPMs

with open("DROME_filt_tpms.tsv", 'w') as f:
    f.write(results_df.to_csv(sep = '\t', index = False))